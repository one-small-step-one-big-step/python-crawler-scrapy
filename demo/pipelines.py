# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import openpyxl
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import pymysql

# 存储到mysql中
class MysqlPipeline(object):
    def __init__(self):
        self.conn = pymysql.connect(user='root', password='ljx386898!~.11',
                                    host='43.138.140.114', port=3306,
                                    database='scrapy', charset='utf8mb4')
        self.cursor = self.conn.cursor()
        self.data = []

    def close_spider(self, spider):
        if len(self.data) > 0:
            self._batch_insert_sql()
        self.conn.close()

    def process_item(self, item, spider):
        # 存储至data中
        self.data.append((item.get("title", ""), item.get("rank", ""), item.get("subject", "")))
        if len(self.data) == 100:
            self._batch_insert_sql()
            self.data.clear()

    #修改为批量执行sql
    def _batch_insert_sql(self):
        insertSql = 'insert into top (`title`, `rank`, `subject`) values (%s, %s, %s)'
        # 批量执行
        self.cursor.executemany(
            insertSql,
            self.data
        )
        self.conn.commit()


'''
存储数据到excel
'''


class DemoPipeline:
    # 初始化执行的方法
    def __init__(self):
        self.wb = openpyxl.Workbook()
        self.ws = self.wb.active
        self.ws.title = "top250"
        self.ws.append(('标题', '评分', '主题'))

    # 爬虫关闭时执行的方法
    def close_spider(self, spider):
        self.wb.save('电影数据.xlsx')

    # 爬取的每一条数据都会执行这个方法
    def process_item(self, item, spider):
        self.ws.append((item.get('title', ''), item.get('rank', ''), item.get('subject', '')))
        return item
